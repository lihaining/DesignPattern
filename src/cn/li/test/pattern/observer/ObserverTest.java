package cn.li.test.pattern.observer;

import cn.li.test.pattern.observer.observer.CurrentConditionsDisplay;
import cn.li.test.pattern.observer.observer.ForecastDisplay;
import cn.li.test.pattern.observer.observer.StatisticsDisplay;
import cn.li.test.pattern.observer.subject.WeatherData;

public class ObserverTest {
    public static void main(String[] args) {
        WeatherData weatherData = new WeatherData();

        CurrentConditionsDisplay currentConditionsDisplay = new CurrentConditionsDisplay(weatherData);
        StatisticsDisplay statisticsDisplay = new StatisticsDisplay(weatherData);
        ForecastDisplay forecastDisplay = new ForecastDisplay(weatherData);
        //模拟气象站
        weatherData.setMeasurements(34.5f,65,345);

    }
}
