package cn.li.test.pattern.observer.observer;

import cn.li.test.pattern.observer.DisplayElement;
import cn.li.test.pattern.observer.Observer;
import cn.li.test.pattern.observer.Subject;

public class CurrentConditionsDisplay implements DisplayElement , Observer {

    private float temperature;
    private float humidity;
    private Subject subject;

    /**
     * 构造器需要主题用来注册
     * @param subject weatherData
     */
    public CurrentConditionsDisplay(Subject subject){
        this.subject = subject;
        subject.registerObserver(this);
    }

    public void unsubscribe(){
        subject.removeObserver(this);
    }

    @Override
    public void display() {
        System.out.println("Current Conditions: "+ temperature + "F degrees and " + humidity + "% humidity");
    }

    @Override
    public void update(float temperature, float humidity, float pressure) {
        this.temperature = temperature;
        this.humidity = humidity;
        display();
    }
}
