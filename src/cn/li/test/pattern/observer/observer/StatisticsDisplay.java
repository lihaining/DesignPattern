package cn.li.test.pattern.observer.observer;

import cn.li.test.pattern.observer.DisplayElement;
import cn.li.test.pattern.observer.Observer;
import cn.li.test.pattern.observer.Subject;

public class StatisticsDisplay implements DisplayElement, Observer {

    private float temperature;
    private float humidity;
    private float pressure;
    private Subject subject;

    /**
     * 构造器需要主题用来注册
     * @param subject weatherData
     */
    public StatisticsDisplay(Subject subject){
        this.subject = subject;
        subject.registerObserver(this);
    }

    public void unsubscribe(){
        subject.removeObserver(this);
    }

    @Override
    public void display() {
        System.out.println("Avg Conditions : temperature="+temperature+"° humitidy="+humidity+"% pressure="+pressure+"pa");
    }

    @Override
    public void update(float temperature, float humidity, float pressure) {
        this.temperature = temperature;
        this.humidity = humidity;
        this.pressure = pressure;

        display();
    }
}
