package cn.li.test.pattern.observer;

public interface DisplayElement {
    void display();
}
