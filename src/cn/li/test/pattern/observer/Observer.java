package cn.li.test.pattern.observer;

public interface Observer {
    void update(float temperature,float humitidy,float pressure);
}
