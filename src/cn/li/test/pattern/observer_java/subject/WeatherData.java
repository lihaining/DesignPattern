package cn.li.test.pattern.observer_java.subject;

import java.util.Observable;

public class WeatherData extends Observable {

    private float temperature;
    private float humidity;
    private float pressure;

    public WeatherData(){}


    /**
     * 当从气象站得到更新观测值时，我们通知观察者
     */
    public void measurementsChanged(){
        setChanged();
        //我们没有用notifyObservers()传递数据对象，这表示我们采用的做法是“拉”
        notifyObservers();
    }

    /**
     * 测试
     * @param temperature 温度
     * @param humidity 湿度
     * @param pressure 气压
     */
    public void setMeasurements(float temperature,float humidity,float pressure){
        this.temperature = temperature;
        this.humidity = humidity;
        this.pressure = pressure;

        measurementsChanged();
    }
    //因为要使用“拉”的方式所以加上get方法
    public float getTemperature() {
        return temperature;
    }

    public float getHumidity() {
        return humidity;
    }

    public float getPressure() {
        return pressure;
    }

}
