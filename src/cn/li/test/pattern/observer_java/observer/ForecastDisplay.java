package cn.li.test.pattern.observer_java.observer;


import cn.li.test.pattern.observer_java.DisplayElement;
import cn.li.test.pattern.observer_java.subject.WeatherData;

import java.util.Observable;
import java.util.Observer;

public class ForecastDisplay implements DisplayElement, Observer {

    private float temperature;
    private float humidity;
    private float pressure;
    private Observable observable;

    /**
     * 构造器需要主题用来注册
     * @param observable weatherData
     */
    public ForecastDisplay(Observable observable){
        this.observable = observable;
        observable.addObserver(this);
    }

    public void unsubscribe(){
        observable.deleteObserver(this);
    }

    @Override
    public void display() {
        System.out.println("Forecast Conditions : temperature="+temperature+"° humitidy="+humidity+"% pressure="+pressure+"pa");
    }

    @Override
    public void update(Observable observable,Object arg) {
        WeatherData weatherData = null;
        if(observable instanceof WeatherData) {
            weatherData = (WeatherData) observable;
        }
        this.temperature = weatherData.getTemperature();
        this.humidity = weatherData.getHumidity();
        this.pressure = weatherData.getPressure();
        display();
    }

}
