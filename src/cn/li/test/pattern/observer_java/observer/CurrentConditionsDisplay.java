package cn.li.test.pattern.observer_java.observer;

import cn.li.test.pattern.observer_java.DisplayElement;
import cn.li.test.pattern.observer_java.subject.WeatherData;

import java.util.Observable;
import java.util.Observer;

public class CurrentConditionsDisplay implements DisplayElement, Observer {

    private float temperature;
    private float humidity;
    private Observable observable;

    /**
     * 构造器需要主题用来注册
     * @param observable weatherData
     */
    public CurrentConditionsDisplay(Observable observable){
        this.observable = observable;
        System.out.println("sss");
        observable.addObserver(this);
    }

    @Override
    public void display() {
        System.out.println("Current Conditions: "+ temperature + "F degrees and " + humidity + "% humidity");
    }

    @Override
    public void update(Observable observable,Object arg) {
        if(observable instanceof WeatherData){
            WeatherData weatherData = (WeatherData) observable;

            this.temperature = weatherData.getTemperature();
            this.humidity = weatherData.getHumidity();
        }
        display();
    }
}
