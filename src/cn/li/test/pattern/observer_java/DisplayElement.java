package cn.li.test.pattern.observer_java;

public interface DisplayElement {
    void display();
}
